﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class BookPage : MonoBehaviour, ITrackableEventHandler, IObserver
{
    #region PRIVATE_MEMBER_VARIABLES

    private TrackableBehaviour mTrackableBehaviour;
    private int count;
    private bool show = false;
    #endregion // PRIVATE_MEMBER_VARIABLES

    #region PUBLIC_VARIABLES
    public GameControler gameControler;
    public page pagePosition;
    public GameObject[] bookPages = new GameObject[9];
    #endregion



    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        gameControler.addObserver(this);
        init();
    }

    private void init()
    {
        count = 0;
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS



    #region PUBLIC_METHODS

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            show = true;

            OnTrackingFound();
            gameControler.foundPage();
        }
        else
        {
            show = false;

            OnTrackingLost();
            gameControler.lostPage(pagePosition);
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
        changeSpace(true);
        bookPages[count].SetActive(true);
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
    }


    private void OnTrackingLost()
    {
        changeSpace(false);
        bookPages[count].SetActive(false);
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }

    private void changeSpace(bool state)
    {
        if (pagePosition == page.RIGHT)
        {
            GameObject[] components = GameObject.FindGameObjectsWithTag("Space Book");
            foreach (GameObject component in components)
            {
                component.GetComponent<Renderer>().enabled = state;
                component.GetComponent<Collider>().enabled = state;
            }
        }
    }

    public void update(UpdateData update)
    {
        if (update.UpdateType == action.RESET)
        {
            init();
        } else if (update.UpdateType == action.PAGE)
        {
            if ((count + update.Page) >= 0 && (count + update.Page) < bookPages.Length)
            {
                if (show)
                {
                    OnTrackingLost();
                }

                count += update.Page;
            }
        }

        if (show)
        {
            OnTrackingFound();
        }
        
    }

    #endregion // PRIVATE_METHODS
}
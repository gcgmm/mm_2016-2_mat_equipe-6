﻿using UnityEngine;
using System.Collections;
using System;

public class PlanetControler : MonoBehaviour, IObserver {

    #region PRIVATE_VARIABLES
    private bool translacao;
    #endregion // PRIVATE_VARIABLES

    #region PUBLIC_VARIABLES
    public GameObject sun;
    public float velocidadeTranslacao;
    public float velocidadeRotacao;
    public double dayPerYear;
    public GameControler gameControler;
    #endregion //PUBLIC_VARIABLES

    #region UNITY_METHODS
    // Use this for initialization
    void Start()
    {
        translacao = true;
        gameControler.addObserver(this);
    }

    // Update is called once per frame
    void Update()
    {
        Transladar();
        Rotacionar();
    }

    void OnMouseOver()
    { }

    void OnMouseExit()
    { }

    #endregion //UNITY_METHODS

    #region PUBLIC_METHODS
    public void Transladar()
    {
        transform.RotateAround(sun.transform.position, transform.up, velocidadeTranslacao * Time.deltaTime);
    }

    public void Rotacionar()
    {
        transform.Rotate(0, velocidadeRotacao * Time.deltaTime, 0);
    }

    public void StopTranlacao()
    {
        translacao = false;
    }

    public void ContinueTranlacao()
    {
        translacao = true;
    }
    #endregion //PUBLIC_METHODS

    public void update(UpdateData update)
    {
        if (update.UpdateType == action.ORIGINAL || update.UpdateType == action.MINUS || update.UpdateType == action.PLUS)
        {
            velocidadeTranslacao = (float)(365.26 * update.Angle / dayPerYear);
        }
    }
}

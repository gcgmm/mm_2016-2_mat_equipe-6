﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class SolarPaneTrackableBehavior : MonoBehaviour, ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES

    private TrackableBehaviour mTrackableBehaviour;

    private int system;
    #endregion // PRIVATE_MEMBER_VARIABLES

    #region PUBLIC_VARIABLES
    #endregion



    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {

        system = 0;
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS



    #region PUBLIC_METHODS

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //OnTrackingFound();
            Show("Space");
            ShowSystem();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS

    /*
    private void OnTrackingFound()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
    }*/


    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }

    public void ChangeSystem()
    {
        system++;
        system %= 2;
        ShowSystem();
    }

    private void ShowSystem()
    {
        switch (system)
        {
            case 0:
                Show("Helio");
                Hide("Geo");
                break;

            case 1:
                Show("Geo");
                Hide("Helio");
                break;
        }
    }

    private void Show(string who)
    {
        Renderer[] rendererComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Collider>(true);
        Light[] lightComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Light>(true);

        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        // Enable light:
        foreach (Light component in lightComponents)
        {
            component.enabled = true;
        }
    }

    private void Hide(string who)
    {
        Renderer[] rendererComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Collider>(true);
        Light[] lightComponents = GameObject.FindGameObjectWithTag(who).GetComponentsInChildren<Light>(true);

        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

        // Enable light:
        foreach (Light component in lightComponents)
        {
            component.enabled = false;
        }
    }
    #endregion // PRIVATE_METHODS
}

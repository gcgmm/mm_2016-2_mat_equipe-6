﻿using UnityEngine;
using System.Collections.Generic;
using System;

public enum action { PLUS, MINUS, ORIGINAL, INFORMATION, INNER, OUTTER, SIZE, PAGE, RESET };
public enum level { SOLAR, DISSECTION, GAME};
public enum planet:int { SUN = 0, MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE };
public enum page { RIGHT, LEFT };

public class GameControler : MonoBehaviour, ISubject {

    #region PRIVATE_METHODS
    private List<IObserver> observers = new List<IObserver>();
    private int yearAngle;
    private UpdateData update = new UpdateData();
    private float[] planetSize = { 140f, 0.49f, 1.21f, 1.27f, 0.67f, 14.2f, 12f, 5.1f, 4.9f };
    private List<float> shown = new List<float>();
    private float timeIn;
    private page pageLost;
    #endregion

    #region PUBLIC_VARIABLE
    public int variationYear;
    public float originalSize;
    #endregion


    #region UNITY_METHODS
    // Use this for initialization
    void Start () {
        yearAngle = 10;

        timeIn = -1;
    }
	
	// Update is called once per frame
	void Update () {
        if (timeIn != -1 && Time.deltaTime - timeIn > 30)
        {
            timeIn = -1;

            update.UpdateType = action.RESET;

            notify();
        }
	}
    #endregion

    #region PUBLIC_METHODS
    public void addVelocity()
    {    
        yearAngle += variationYear;

        update.UpdateType = action.PLUS;
        update.Angle = yearAngle;
        notify();
    }

    public void minusVelocity()
    {
        if (yearAngle - variationYear > 0)
        {
            yearAngle -= variationYear;

            update.UpdateType = action.MINUS;
            update.Angle = yearAngle;

            notify();
        }
    }

    public void changeSize(planet who)
    {
        shown.Add(planetSize[(int)who]);
        //shown.Sort((a, b)=> (int)(b-a));
        shown.Sort();

        update.UpdateType = action.SIZE;
        update.Size = shown[shown.Count - 1];
        Debug.Log(shown[shown.Count - 1]);
        notify();
    }

    public void removeSize(planet who)
    {
        shown.Remove(planetSize[(int)who]);
        //shown.Sort((a, b) => (int)(b - a));
        shown.Sort();

        if (shown.Count > 0)
        {
            update.UpdateType = action.SIZE;
            update.Size = shown[shown.Count - 1];
            Debug.Log(shown[shown.Count - 1]);
            notify();
        }
    }

    public void originalVelocity()
    {
        yearAngle = 10;

        update.UpdateType = action.ORIGINAL;
        update.Angle = yearAngle;

        notify();
    }

    public void changeInformation(bool show)
    {
        update.UpdateType = action.INFORMATION;
        update.Show = show;

        notify();
    }

    public void changeInnerside(bool show)
    {
        update.UpdateType = action.INNER;
        update.Show = show;

        notify();
    }

    public float[] getPlanetSize()
    {
        return planetSize;
    }

    public void foundPage()
    {
        if (timeIn != -1)
        {
            update.UpdateType = action.PAGE;
            if (pageLost == page.RIGHT)
            {
                update.Page = 1;
            }
            else
            {
                update.Page = -1;
            }
            timeIn = -1;
            notify();
        }
    }

    public void lostPage(page who)
    {
        if (timeIn == -1)
        {
            pageLost = who;
            timeIn = Time.deltaTime;
        }
    }

    #endregion

    #region PRIVATE_METHODS

    private void ChangeRender(GameObject objectToActive, bool state)
    {
        if (objectToActive.GetComponent<Renderer>() != null) { objectToActive.GetComponent<Renderer>().enabled = state; }

        if (objectToActive.GetComponent<Collider>() != null) { objectToActive.GetComponent<Collider>().enabled = state; }
    }
    #endregion

    #region OBSERVER_METHODS
    public void addObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    public void removeObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    public void notify()
    {
        foreach (IObserver observer in observers)
        {
            observer.update(update);
        }
    }
    #endregion
}

﻿
public enum type { DISSECTION, INFORMATION, VELOCITY };
// tipo de update que vai ser necessário
public class UpdateData {

    private action _updateType;
    public action UpdateType
    {
        get { return _updateType; }
        set { _updateType = value; }
    }

    private int _angle;
    public int Angle
    {
        get { return _angle; }
        set { _angle = value; }
    }

    private bool _show;
    public bool Show
    {
        get { return _show; }
        set { _show = value; }
    }

    private float _size;
    public float Size
    {
        get { return _size; }
        set { _size = value; }
    }

    private int _page;
    public int Page
    {
        get { return _page; }
        set { _page = value; }
    }
}

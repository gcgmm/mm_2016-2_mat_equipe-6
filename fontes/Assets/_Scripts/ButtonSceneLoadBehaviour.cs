﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.SceneManagement;

public class ButtonSceneLoadBehaviour : MonoBehaviour, Vuforia.IVirtualButtonEventHandler
{
    #region PUBLIC_VARIABLES
    public GameControler gameControler;
    #endregion

    // Use this for initialization
    void Start()
    {
        VirtualButtonBehaviour[] vb = GetComponentsInChildren<VirtualButtonBehaviour>();
        foreach (VirtualButtonBehaviour vbb in vb)
        {
            vbb.RegisterEventHandler(this);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        switch (vb.VirtualButtonName)
        {
            case "solar":
                SceneManager.LoadScene("Solar", LoadSceneMode.Single);
                break;
            case "dissection":
                SceneManager.LoadScene("Dissection", LoadSceneMode.Single);
                break;
            case "game":
                //SceneManager.LoadScene("Solar", LoadSceneMode.Single);
                break;
        }
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {

    }
}

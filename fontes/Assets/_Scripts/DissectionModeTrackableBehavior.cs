﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System;

public class DissectionModeTrackableBehavior : MonoBehaviour, ITrackableEventHandler, IObserver
{
    #region PRIVATE_MEMBER_VARIABLES

    private TrackableBehaviour mTrackableBehaviour;
    private bool displayInformation = false;
    private bool displayInner = false;
    private bool display = false;
    private float size;
    #endregion // PRIVATE_MEMBER_VARIABLES

    #region PUBLIC_VARIABLES
    public GameControler gameControler;
    public planet who;
    #endregion



    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        gameControler.addObserver(this);
        size = gameControler.originalSize;
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS



    #region PUBLIC_METHODS

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            gameControler.changeSize(who);
            OnTrackingFound();
            display = true;
        }
        else
        {
            gameControler.removeSize(who);
            OnTrackingLost();
            display = false;
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        if (displayInformation)
        {
            showInformation();
        } else
        {
            hideInformation();
        }

        if (displayInner)
        {
            showInnerside();
        }
        else
        {
            hideInnerside();
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
    }


    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }



    private void showInformation()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform transformC in transforms)
        {
            if (transformC.tag == "Information")
            {
                ChangeRender(transformC.gameObject, true);
            }
        }
    }

    private void hideInformation()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform transformC in transforms)
        {
            if (transformC.tag == "Information")
            {
                ChangeRender(transformC.gameObject, false);
            }
        }
    }

    private void showInnerside()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform transformC in transforms)
        {
            if (transformC.tag == "Planet")
            {
                ChangeRender(transformC.gameObject, false);
            }
        }


        MeshRenderer[] meshRender = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in meshRender)
        {
            if (mesh.tag == "Planet Dissection")
            {
                mesh.enabled = true;
            }
        }
    }

    private void hideInnerside()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform transformC in transforms)
        {
            if (transformC.tag == "Planet")
            {
                ChangeRender(transformC.gameObject, true);
            }
        }


        MeshRenderer[] meshRender = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in meshRender)
        {
            if (mesh.tag == "Planet Dissection")
            {
                mesh.enabled = false;
            }
        }
    }

    private void ChangeRender(GameObject objectToActive, bool state)
    {
        if (objectToActive.GetComponent<Renderer>() != null) { objectToActive.GetComponent<Renderer>().enabled = state; }

        if (objectToActive.GetComponent<Collider>() != null) { objectToActive.GetComponent<Collider>().enabled = state; }
    }

    public void update(UpdateData update)
    {
        if (update.UpdateType == action.INFORMATION)
        {
            displayInformation = update.Show;
        } else if (update.UpdateType == action.INNER)
        {
            displayInner = update.Show;
        } else if (update.UpdateType == action.SIZE)
        {
            if (gameControler.getPlanetSize()[(int)who] <= update.Size)
            {
                size = (gameControler.getPlanetSize()[(int)who] * gameControler.originalSize) / update.Size;
                sizeChange();
            }
        }

        if (display)
        {
            OnTrackingFound();
        }
    }

    private void sizeChange()
    {
        Transform[] transforms = GetComponentsInChildren<Transform>();
        foreach (Transform transformC in transforms)
        {
            if (transformC.tag == "Planet")
            {
                transformC.localScale = new Vector3(size, size, size);
            } else if (transformC.tag == "Planet Dissection")
            {
                if (transformC.childCount > 0)
                {
                    transformC.localScale = new Vector3(size/2, size/2, size/2);
                }
            }
        }
    }

    #endregion // PRIVATE_METHODS
}
﻿using UnityEngine;
using System.Collections;

public class PlanetSeasonControler : MonoBehaviour {


    public float velocidadeTranslacao;
    public float velocidadeRotacao;
    float angle;

    // Use this for initialization
    void Start()
    {
        angle = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
            angle += velocidadeTranslacao * Time.deltaTime;
            if (angle >= 360)
            {
                angle = 0;
            }
            Rotacionar();
            Eixo();
    }

    public void Rotacionar()
    {
        transform.Rotate(0, velocidadeRotacao * Time.deltaTime, 0);
    }

    public void Eixo()
    {
        float velocidade = 0;
        if (angle < 180)
        {
            velocidade = Mathf.Abs(velocidadeTranslacao);
        }
        else if (angle > 180)
        {
            velocidade = Mathf.Abs(velocidadeTranslacao) * -1;
        }
        transform.Rotate(0, 0, 0.2606f * velocidade * Time.deltaTime, Space.World);
    }

}

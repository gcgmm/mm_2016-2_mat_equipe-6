﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Vuforia;

public class ButtonVelocityBehaviour : MonoBehaviour, Vuforia.IVirtualButtonEventHandler
{
    #region PUBLIC_VARIABLES
    public GameControler gameControler;
    #endregion

    // Use this for initialization
    void Start()
    {
        VirtualButtonBehaviour[] vb = GetComponentsInChildren<VirtualButtonBehaviour>();
        foreach (VirtualButtonBehaviour vbb in vb)
        {
            vbb.RegisterEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        switch (vb.VirtualButtonName)
        {
            case "increase":
                gameControler.addVelocity();
                break;
            case "decrease":
                gameControler.minusVelocity();
                break;
            case "original":
                gameControler.originalVelocity();
                break;
        }
        Debug.Log("Apertou");
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {

    }
}